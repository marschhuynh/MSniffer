from helper.utils import get_mac_addr
import struct
import socket


class Ethernet:

    def __init__(self, data):

        dest, src, prototype = struct.unpack('! 6s 6s H', data[:14])

        self.dest = get_mac_addr(dest)
        self.src  = get_mac_addr(src)
        self.protocol = socket.htons(prototype)
        self.data = data[14:]
