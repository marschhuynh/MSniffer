#! /usr/bin/python3
import socket
from helper.ethernet import *
from helper.ipv4 import *
from helper.tcp import *
from helper.udp import *
from helper.http import *

from PyQt5 import QtCore, QtGui, QtWidgets
from GUI import Ui_MainWindow
from MyGUI import MyGUI
from threading import Thread
from queue import Queue

def sniff(queue, cond):
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    stop = False
    while True:
        if not stop:
            raw_data, addr = conn.recvfrom(65535)
            queue.put((raw_data, addr))
            if not cond.empty():
                stop = cond.get()


def transfer(queue, ui, cond):
    stop = False
    while True:
        if not stop:
            raw_data, addr = queue.get()

            eth_frame = Ethernet(raw_data)
            print("\nEthernet Frame:")
            print("Destination: {}, Source: {}, Protocol: {}".format(eth_frame.dest, eth_frame.src, eth_frame.protocol))

            adata = []
            # Ethernet Frame
            adata.append(eth_frame.dest)
            adata.append(eth_frame.src)
            if eth_frame.protocol == 8:
                adata.append("Ipv4")
            else:
                adata.append(str(eth_frame.protocol))

            # IpV4
            if eth_frame.protocol == 8:
                ipv4 = IpV4(eth_frame.data)

                adata.append(str(ipv4.version))
                adata.append(str(ipv4.header_length))
                adata.append(str(ipv4.ttl))
                if ipv4.proto == 6:
                    adata.append("TCP")
                elif ipv4.proto == 17:
                    adata.append("UDP")
                else:
                    adata.append("Other")
                adata.append(ipv4.src)
                adata.append(ipv4.target)

                if ipv4.proto == 6:
                    tcp = TCP(ipv4.data)

                    adata.append(str(tcp.src_port))
                    adata.append(str(tcp.dest_port))
                    adata.append(str(tcp.sequence))
                    adata.append(str(tcp.acknowledgment))
                    adata.append(str(tcp.acknowledgment))

                    adata.append(str(tcp.flag_urg))
                    adata.append(str(tcp.flag_ack))
                    adata.append(str(tcp.flag_psh))
                    adata.append(str(tcp.flag_rst))
                    adata.append(str(tcp.flag_syn))
                    adata.append(str(tcp.flag_fin))
                    result = ""
                    if len(tcp.data) > 0:

                        # HTTP
                        if tcp.src_port == 80 or tcp.dest_port == 80:
                            try:
                                http = HTTP(tcp.data)
                                http_info = str(http.data).split('\n')
                                for line in http_info:
                                    result += str(line) + "\n"

                            except:
                                result = str(tcp.data)
                                result = str("Error")
                        else:
                            result = str(tcp.data)

                    adata.append(result)

                elif ipv4.proto == 17:
                    udp = UDP(ipv4.data)
                    adata.append(str(udp.src_port))
                    adata.append(str(udp.dest_port))
                    adata.append(str(udp.size))

            ui.appendRowTable([adata])


def gui(ui):
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui.setupUi(MainWindow)
    Ok = True
    MainWindow.show()
    sys.exit(app.exec_())

def run():
    ui = MyGUI()
    q = Queue()
    cond = Queue()

    sniff_process = Thread(target = sniff, args=(q,cond))
    transfer_process = Thread(target = transfer, args=(q, ui, cond))
    gui_process = Thread(target = gui, args=(ui, ))

    gui_process.start()
    sniff_process.start()
    transfer_process.start()

if __name__ == '__main__':
    run()
