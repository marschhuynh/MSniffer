from GUI import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets


class MyGUI(Ui_MainWindow):

    def setupUi(self, MainWindow):
        super().setupUi(MainWindow)
        self.btn_start.clicked.connect(self.handle_btn_start_clicked)
        self.btn_stop.clicked.connect(self.handle_btn_stop_clicked)
        self.btn_clear.clicked.connect(self.handle_btn_clear_clicked)
        self.btn_filter.clicked.connect(self.handle_btn_filter_clicked)
        self.table_package.entered.connect(self.handle_table_view_selectRow)
        self.table_package.clicked.connect(self.handle_table_view_selectRow)

        model = QtGui.QStandardItemModel(self.table_package)

        model.setHorizontalHeaderLabels(["Dest","Source","2","3", "Data","5","Type","Ip Source","Ip destination","9","10","11","12","13","14","15","16","17","18","19","20","21"])

        self.table_package.resizeColumnsToContents()
        self.table_package.setModel(model)

        self.table_package.setColumnHidden(2,True)
        self.table_package.setColumnHidden(4,True)
        self.table_package.setColumnHidden(5,True)
        self.table_package.setColumnHidden(3,True)
        self.table_package.setColumnHidden(9,True)
        self.table_package.setColumnHidden(10,True)
        self.table_package.setColumnHidden(11,True)
        self.table_package.setColumnHidden(12,True)
        self.table_package.setColumnHidden(13,True)
        self.table_package.setColumnHidden(14,True)
        self.table_package.setColumnHidden(15,True)
        self.table_package.setColumnHidden(16,True)
        self.table_package.setColumnHidden(17,True)
        self.table_package.setColumnHidden(18,True)
        self.table_package.setColumnHidden(19,True)
        self.table_package.setColumnHidden(20,True)
        self.table_package.setColumnHidden(21,True)

        self.table_package.horizontalHeader().setSectionResizeMode(3)

        self.retranslateUi(MainWindow)


    def handle_btn_start_clicked(self):
        print("Start")

    def handle_btn_filter_clicked(self):
        print("Filter")

    def handle_btn_stop_clicked(self):
        print("Stop")

    def handle_btn_clear_clicked(self):
        print("Clear")

    def handle_table_view_selectRow(self, clickedIndex):
        row = clickedIndex.row()
        mac_dest = clickedIndex.sibling(row,0).data()
        mac_src = clickedIndex.sibling(row,1).data()
        eth_protocol = clickedIndex.sibling(row,2).data()

        data = "Empty"

        if eth_protocol == "Ipv4":
            data = '\nEthernet Frame'
            data += '\n   Destination: {}, Source: {}, Protocol: {}'.format(mac_dest, mac_src, eth_protocol)

            version = clickedIndex.sibling(row,3).data()
            header_length = clickedIndex.sibling(row,4).data()
            ttl = clickedIndex.sibling(row,5).data()
            ip_proto = clickedIndex.sibling(row,6).data()
            ip_src = clickedIndex.sibling(row,7).data()
            ip_target = clickedIndex.sibling(row,8).data()

            data += '\n   IPv4 Packet:'
            data += '\n       Version: {}, Header Length: {}, TTL: {},'.format(version,header_length, ttl)
            data += '\n       Protocol: {}, Source: {}, Target: {}'.format(ip_proto, ip_src, ip_target)

            if ip_proto == "TCP":
                src_port = clickedIndex.sibling(row,9).data()
                dest_port = clickedIndex.sibling(row,10).data()
                sequence = clickedIndex.sibling(row,11).data()
                acknowledgment = clickedIndex.sibling(row,12).data()

                flag_urg = clickedIndex.sibling(row,13).data()
                flag_ack = clickedIndex.sibling(row,14).data()
                flag_psh = clickedIndex.sibling(row,15).data()
                flag_rst = clickedIndex.sibling(row,16).data()
                flag_syn = clickedIndex.sibling(row,17).data()
                flag_fin = clickedIndex.sibling(row,18).data()
                tcp_data = clickedIndex.sibling(row,20).data()
                # tcp2_data = clickedIndex.sibling(row,21).data()

                data += '\nTCP Segment:'
                data += '\n       Source Port: {}, Destination Port: {}'.format(src_port, dest_port)
                data += '\n       Sequence: {}, Acknowledgment: {}'.format(sequence, acknowledgment)
                data += '\n       Flags:'
                data += '\n           URG: {}, ACK: {}, PSH: {}'.format(flag_urg, flag_ack, flag_psh)
                data += '\n           RST: {}, SYN: {}, FIN:{}'.format(flag_rst, flag_syn, flag_fin)

                if src_port == "80" or dest_port == "80":
                    data += '\nHTTP Data:\n'
                else:
                    data += '\nTCP Data:\n'
                data += tcp_data

                data += "\n"

            elif ip_proto == "UDP":

                data += '\nUDP Segment:'
                src_port = clickedIndex.sibling(row,9).data()
                dest_port = clickedIndex.sibling(row,10).data()
                size = clickedIndex.sibling(row,11).data()
                data += '\n       Source Port: {}, Destination Port: {}, Length: {}'.format(src_port, dest_port, size)
            else:
                data += '\nEthernet Data'

        if data != "Empty":
            self.package_view.setPlainText(data)

    def appendRowTable(self, data):
        model = self.table_package.model()
        for row in data:
            data_row = []
            for item in row:
                cell = QtGui.QStandardItem(item)
                data_row.append(cell)
            model.appendRow(data_row)
